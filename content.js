js:
var d = [];
MY_URL = MY_URL.split("##")[1];
let headers = {
    "User-Agent": MOBILE_UA
};

let html = fetch(MY_URL, {
    timeout: 3000,
    headers: headers
});

let title = "";
let cont = "";
if (/ikanshuba|woo15/.test(MY_URL)) {
    title = '.text-center||.articlename&&Text'
    cont = '#pt-pop||#content&&Html'
} else
if (/xbyuan|iwurexs/.test(MY_URL)) {
    title = '#_bqgmb_h1||#nr_h1&&Text'
    cont = '#nr1&&Html'
} else
if (/xs7|75zhongwen/.test(MY_URL)) {
    title = '.headline&&Text'
    cont = '.content&&Html'
} else
if (/fanqianxs/.test(MY_URL)) {
    title = '.title&&Text'
    cont = '#text&&Html'
} else
if (!/midukanshu|api|pigqq|pysmei/.test(MY_URL)) {
    title = '.chapter-name||#chaptertitle&&Text'
    cont = '#CongWenCha||#novelcontent&&p&&Html'
}

let 章节 = pdfh(html, title);

let cond = pdfh(html, cont);

function 净化() {
    return cond.replace(/\s+/g, "").replace(/\&nbsp\;/g, "").replace(/<br>|<p>|\n/g, "<br>\t\t\t\t\t\t").replace(/<\/br>|<\/p>/g, "").replace(/(\w+)\.(\w+)\.(\w+)|http(s)?:\/\/.*\.[a-zA-Z]|<a.*\/\">|\（本章未完.*\）|最新网址\：|精华书阁.*章节！|紧急通知\：精华书阁.*报错~』/g, "")
};

let content = 净化();

let con = cond.replace(/\（/g, "(").replace(/\）/g, ")").replace(/\s+/g, "");
let 章 = 章节.replace(/\（/g, "(").replace(/\）/g, ")").replace(/\s+/g, "");
let cs = "";
if (/\(第1\/|\(1\//.test(章)) {
    cs = /\(1\"/.test(章) ? 章.split("(1/") : 章.split("(第1/");
} else {
    cs = /\(1\"/.test(con) ? con.split("(1/") : con.split("(第1/");
}
//log("cs: " + cs.length)
if (cs.length > 1) {
    try {
        let count = parseInt(cs[1].split(")")[0])
        //log("count: " + count)
        if (count > 1) {
            let urls = []
            for (let i = 1; i < count; i++) {
                urls.push({
                    url: /wenxuedu/.test(getUrl()) ? getUrl().replace(/\/(\d+)\/(\d+)\//g, "/$1/$2_" + (i + 1) + "/") : getUrl().replace(".html", "_" + (i + 1) + ".html"),
                    options: {
                        headers: headers
                    }
                })
            }
            let htmls = batchFetch(urls)
            for (let it of htmls) {
                if (it == "") {
                    continue
                }
                cond = pdfh(it, cont);
                content = content + 净化();
            }

        }
    } catch (e) {}
};
if (/midukanshu|api|pigqq|pysmei/.test(MY_URL)) {
    if (/17k|pigqq|pysmei/.test(MY_URL)) {
        let list = JSON.parse(html).data;
        let name = /17k/.test(MY_URL) ? list.name : list.cname;
        d.push({
            title: "<b><big>" + name + "</big></b>",
            col_type: "rich_text"
        }, {
            title: list.content.replace(/\n|\r|\s+/g, "<br>\t\t\t\t\t\t"),
            col_type: "rich_text"
        })
    } else {
        let list = JSON.parse(html);
        for (let i in list) {
            d.push({
                title: "\t\t\t\t\t\t" + list[i].content,
                col_type: "rich_text"
            })
        }
    }
} else {
    d.push({
        title: '<b><big>' + 章节 + "</big></b><br>" + content,
        col_type: "rich_text"
    })
}
setResult(d);