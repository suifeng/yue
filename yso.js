js:
var d = []
addListener("onClose", $.toString(() => {
    clearMyVar("Mysou");
    clearMyVar("bak");
    clearMyVar("影搜");
}));
putMyVar("影搜", "1");
let sokey = getMyVar("Mysou");
let page = MY_PAGE;
let headers = {
    "User-Agent": MOBILE_UA,
    "Referer": MY_URL,
};
let rules = {
    "精华书阁": {
        ssurl: 'https://m.xbyuan.com/search.html?ie=utf-8&word=' + sokey,
    },
    "爱看书吧": {
        ssurl: 'http://www.ikanshuba.com/search/result.html?searchkey=' + sokey + '&page=fypage',
    },
    "采墨阁": {
        ssurl: 'https://m.caimoge.com/search/#searchkey=' + sokey + '&submit=',
    },
    "4020小说": {
        ssurl: 'https://m.iwurexs.com/search.php?searchkey=' + sokey + '&submit=',
    },
    "小说旗": {
        ssurl: 'http://m.xs7.cc/#http://m.xs7.cc/search.html#searchkey=' + sokey + '#fypage#',
    },
    "文学度": {
        ssurl: 'http://m.wenxuedu.com/modules/article/search.php?q=' + sokey,
    },
    "Wo15": {
        ssurl: 'https://m.woo15.com/s.php/#searchkey=' + sokey,
    },
    "番茄小说": {
        ssurl: 'https://m.fanqianxs.com/api/search?q=' + sokey,
    },
    "米读api": {
        ssurl: 'http://api.midukanshu.com/fiction/search/search#device=865166023475380&app=midu&keyword=' + sokey + '&page=fypage',
    },
    "十七开api": {
        ssurl: 'http://api.ali.17k.com/v2/book/search?app_key=4037465544&sort_type=0&page=fypage&class=0&key=' + sokey + '&_versions=971&client_type=1&_filter_data=1&channel=2&merchant=17KH5&_access_version=2&cps=0',
    },
    "笔趣阁api": {
        ssurl: 'https://souxs.pigqq.com/search.aspx?page=fypage&siteid=app2&key=' + sokey,
    },
    "猴子api": {
        ssurl: 'https://souxs.leeyegy.com/search.aspx?key=' + sokey + '&page=fypage&siteid=app2',
    },

};

let rn0 = Object.keys(rules);
let rn = "";
if (getItem("隐藏", "off") == "on") {
    rn = rn0;
} else {
    rn = rn0;
    rn.splice(0, 0);
};
let selected = getItem("s0", rn[0]);
let rule = rules[selected];
try {
    var ssurl = rule.ssurl;
} catch (e) {
    clearItem("s0");
    refreshPage(false);
};
let ssurl = rule.ssurl;
let jusou = getItem("s1", "0") == "1";
let col = getItem("h1", "竖列") == "竖列" ? "movie_1_vertical_pic" : getItem("h1", "竖列") == "双列" ? "movie_2" : "movie_3_marquee";
let slie = getItem("h1", "竖列") == "竖列";
if (MY_PAGE == 1) {
    let lmtu = "https://lanmeiguojiang.com/tubiao/q/";
    let lie = ["竖列", "双列", "三列"];
    let bia = lie.map((tit) => {
        return tit == getItem("h1", "竖列") ? '““<b>' + tit + '</b>””' : tit;
    });
    d.push({
        title: "<b>" + getItem("h1", "竖列") + "排版</b>",
        url: $(bia, 3, "切换排版").select(() => {
            setItem("h1", input.replace(/\“|\”|\<b\>|\<\/b\>/g, ""));
            refreshPage(false);
            return "hiker://empty"
        }),
        img: lmtu + "50.png",
        col_type: "icon_small_3"
    });
    d.push({
        title: jusou ? "<b>聚合搜索</b>" : "<b>选源搜索</b>",
        url: $("#noLoading#").lazyRule(() => {
            if (getItem("s1", "0") == "1") {
                clearItem("s1");
            } else {
                setItem("s1", "1");
            };
            refreshPage(false);
            return "hiker://empty";
        }),
        img: jusou ? lmtu + "123.png" : lmtu + "103.png",
        col_type: "icon_small_3"
    });

    if (MY_NAME == "海阔视界" && getAppVersion() >= "3635" || MY_NAME == "嗅觉浏览器" && getAppVersion() >= "988") {
        d.push({
            title: getSearchMode() == "0" ? "<b>默认模式</b>" : "<b>精准模式</b>",
            url: $("#noLoading#").lazyRule(() => {
                if (getSearchMode() == "0") {
                    setSearchMode(1);
                } else {
                    setSearchMode(0);
                };
                refreshPage(false);
                return "hiker://empty";
            }),
            img: getSearchMode() == "0" ? http + "tu/moren.png" : http + "tu/jing.png",
            col_type: "icon_small_3"
        });
    } else {
        d.push({
            title: "<b>默认模式</b>",
            url: "toast://精准模式需更新最新版海阔",
            img: http + "tu/moren.png",
            col_type: "icon_small_3"
        });
    };

    d.push({
        col_type: "line_blank"
    })
    if (!jusou) {
        d.push({
            title: '<small>当前已选\t\t' + '<b><font color="#FA7298">' + selected + '</font></b>' + '\t\t搜索</small>',
            url: "hiker://empty",
            img: "https://lanmeiguojiang.com/tubiao/more/299.png",
            col_type: "avatar"
        })

        for (xuan of rn) {
            d.push({
                title: xuan == selected ? '‘‘’’<small><b><font color="#FA7298">' + xuan + '</font></b></small>' : '‘‘’’<small>' + xuan + '</small>',
                url: xuan + $("#noLoading#").lazyRule(() => {
                    setItem("s0", input);
                    refreshPage(false);
                    return "hiker://empty";
                }),
                col_type: "scroll_button"
            })
        }
    };

    d.push({
        title: "搜索",
        desc: "可 以 少 字 ，不 可 错 字 ！",
        url: $.toString(() => {
            putMyVar("Mysou", input);
            if (getMyVar("Mysou")) {
                return input + $("#noLoading#").lazyRule(() => {
                    require(config.依赖);
                    return sourl()
                });
            } else {
                return "hiker://empty";
            };
        }),
        col_type: "input",
        extra: {
            defaultValue: sokey,
            id: "solx",
            onChange: $.toString((sokey) => {
                if (input.length == 0) {
                    deleteItemByCls("newz");
                }
                if (input.length > 0 && input !== sokey) {
                    deleteItemByCls("newz");
                    let html = request("https://movie.douban.com/j/subject_suggest?q=" + input, {
                        timeout: 3000
                    });
                    let list = JSON.parse(html) || [];
                    let newz = list.map(li => {
                        try {
                            return {
                                title: li.title,
                                desc: "年份：" + li.year,
                                url: li.title + $("#noLoading#").lazyRule(() => {
                                    require(config.依赖);
                                    return sourl()
                                }),
                                col_type: "text_1",
                                extra: {
                                    cls: "newz"
                                }
                            }
                        } catch (e) {}
                    });
                    if (newz.length > 0) {
                        addItemAfter("solx", newz);
                    }
                }
            }, sokey)
        }
    });
    d.push({
        col_type: "big_blank_block"
    })
    d.push({
        col_type: "line_blank"
    })
    d.push({
        col_type: "big_blank_block"
    })
    if (getMyVar("Mysou")) {
        let erji = MY_PARAMS.erji;
        d.push({
            title: '<small>搜索\t\t' + '<b><font color="#FA7298">' + getMyVar("Mysou") + '</font></b>' + '\t\t结果' + '</small>',
            desc: '<small><b><font color="#FA7298">返回>></font></b></small>\t\t',
            url: $("#noLoading#").lazyRule((erji) => {
                if (erji == "1") {
                    back(false);
                } else {
                    clearMyVar("Mysou");
                    refreshPage();
                }
                return "hiker://empty"
            }, erji),
            img: "https://lanmeiguojiang.com/tubiao/more/23.png",
            col_type: "avatar"
        })
    }
}
//log("before");
if (getMyVar("Mysou")) {
    function search(name, ssurl, d, page) {
        try {
            ssurl = ssurl.replace("fypage", page);
            putMyVar("ssurl", ssurl);
            let MY_HOME = getHome(ssurl);
            let MY_URL = ssurl;
            let jin = getSearchMode() == "1";

            if (/caimoge|woo15|midukanshu/.test(MY_URL)) {
                let M = MY_URL.split('#');
                html = request(M[0], {
                    body: M[1],
                    method: 'POST',
                    timeout: 3000,
                    headers: /midukanshu/.test(MY_URL) ? {
                        "content-type": "application/x-www-form-urlencoded",
                        "user-agent": ""
                    } : headers
                }); //log(html)
            } else
            if (/xs7|75zhongwen/.test(MY_URL)) {
                let M = MY_URL.split('#');
                let syo = JSON.parse(fetch(M[1], {
                    redirect: false,
                    withHeaders: true,
                    body: M[2],
                    method: 'POST',
                    headers: headers
                }));//log(syo)
                let S = syo.headers.location[0]; 
                html = fetch((/xs7|75zhongwen/.test(MY_URL) ? M[0] + S.replace(/1.html/, page + ".html") : M[0] + S), {
                    timeout: 3000,
                    headers: headers
                }); //log(html)
            } else {
                html = fetch(ssurl, {
                    timeout: 3000,
                    headers: /leeyegy|pigqq/.test(MY_URL) ? {
                        "content-type": "application/x-www-form-urlencoded",
                         "Accept-Language": "zh-CN,zh;q=0.8",
                        "User-Agent": MOBILE_UA,
                    } : headers
                });
            };
            if (html.indexOf("检测中") != -1) {
                html = request(MY_HOME + "?btwaf" + html.match(/btwaf(.*?)\"/)[1], {
                    headers: headers
                });
            };

            if (/验证码|系统安全验证|验证后/.test(html)) {
                require(http + "yzm.js");
                evalPrivateJS(ssyz);

                let courl = MY_HOME + (MY_URL.indexOf("search-pg-1-wd-") > -1 ? "/inc/common/code.php?a=search" : MY_URL.indexOf("auete") > -1 ? "/include/vdimgck.php" : "/index.php/verify/index.html?");
                let vcode = getVCode2(courl, JSON.stringify(headers), "num");

                if (MY_URL.indexOf("auete") > -1) {
                    fetch(MY_HOME + "/search.php?scheckAC=check&page=&searchtype=&order=&tid=&area=&year=&letter=&yuyan=&state=&money=&ver=&jq=", {
                        headers: headers,
                        body: "validate=" + JSON.parse(vcode).ret + "&searchword=",
                        method: "POST"
                    });
                } else {
                    fetch(MY_HOME + (MY_URL.indexOf("search-pg-1-wd-") > -1 ? "/inc/ajax.php?ac=code_check&type=search&code=" : html.match(/\/index.php.*?verify=/)[0]) + JSON.parse(vcode).ret, {
                        headers: headers,
                        method: MY_URL.indexOf("search-pg-1-wd-") > -1 ? "GET" : "POST"
                    });
                };

                let cod = JSON.parse(vcode).ret;
                if (/[a-zA-Z]/.test(cod) && !/auete/.test(MY_URL) || /\D/.test(cod) && !/auete/.test(MY_URL) || cod.length != "4") {
                    d.push({
                        title: "验证码似乎有误 " + "““点击重试””",
                        url: $('#noLoading#').lazyRule(() => {
                            refreshPage(true);
                            return "hiker://empty";
                        }),
                        col_type: "text_center_1"
                    })
                } else {
                    let time = /bilituys/.test(MY_URL) ? 5000 : 3000;
                    java.lang.Thread.sleep(time);
                    html = fetch(MY_URL, {
                        headers: headers,
                        timeout: 3000
                    });
                };
            };
            //log(html)

            //列表
            let 列表 = "";
            if (/xbyuan/.test(MY_URL)) {
                列表 = 'body&&.searchbook'
            } else 
            if (/xs7|75zhongwen/.test(MY_URL)) {
                列表 = '.list&&li'
            } else
            if (/searchresult|topul/.test(html)) {
                列表 = '.searchresult||.topul&&p||li'
            } else
            if (/searchbook|bookbox/.test(html)) {
                列表 = 'body&&.searchbook||.bookbox'
            } else
            if (/pt-recomment/.test(html)) {
                列表 = '.pt-recomment&&.pt-recomment-cont'
            }

            //更新
            let 更新 = "";
            try {
                if (/youmengyusheng/.test(MY_URL)) {
                    更新 = 'a,1&&Text'
                } else
                if (/xs7|75zhongwen/.test(MY_URL)) {
                    更新 = 'p,3&&a&&Text'
                } else
                if (/color2|updatelast/.test(html)) {
                    更新 = '.color2||.updatelast&&Text'
                } else
                if (/update/.test(html)) {
                    更新 = '.update&&Text'
                } else
                if (/booknews/.test(html)) {
                    更新 = '.booknews&&label,2&&Text'
                } else {
                    更新 = '.author&&Text'
                }
            } catch (e) {};

            //作者
            let 作者 = "";
            try {
                if (/youmengyusheng/.test(MY_URL)) {
                    作者 = 'Text[2]'
                } else 
                if (/xs7|75zhongwen/.test(MY_URL)) {
                    作者 = 'p,1&&a&&Text'
                } else
                if (/color5|book_author/.test(html)) {
                    作者 = '.color5||.book_author&&Text'
                } else
                if (/booknews/.test(html)) {
                    作者 = '.booknews&&label&&Text'
                } else {
                    作者 = '.author&&Text'
                }
            } catch (e) {};
            //简介
            let 简介 = "";
            try {
                if (/xs7|75zhongwen|woo15/.test(MY_URL)) {
                    简介 = 'p,2||p&&Text'
                } else
                if (/color4/.test(html)) {
                    简介 = '.color4&&Text'
                } else {
                    简介 = '.intro_line&&Text'
                }
            } catch (e) {};
            //类型
            let 类型 = "";
            try {
                if (/xs7|75zhongwen/.test(MY_URL)) {
                    类型 = 'p,1&&span,0&&Text'
                } else
                if (/book_flag/.test(html)) {
                    类型 = '.book_flag&&Text'
                } else
                if (/booknews/.test(html)) {
                    类型 = '.booknews&&label,1&&Text'
                } else {
                    类型 = '.pt-recomment-label&&Text'
                }
            } catch (e) {};

            //图片
            let 图片 = "";
            try {
                if (/t-ui/.test(MY_URL)) {
                    图片 = '.lazy&&data-original'
                } else
                if (/sstv/.test(MY_URL)) {
                    图片 = 'img&&src'
                } else {
                    图片 = 'img&&data-original||data-src||lay-src||src'
                }
            } catch (e) {};
            //片名
            let 片名 = "";
            let 链接 = "";
            try {
                if (/youmengyusheng/.test(MY_URL)) {
                    片名 = 'a,0&&Text'
                    链接 = 'a&&href'
                } else
                if (/bookname|color7/.test(html)) {
                    片名 = '.bookname||.color7&&Text'
                    链接 = 'a&&href'
                } else {
                    片名 = 'h3||h2||a&&Text'
                    链接 = 'a&&href'
                }
            } catch (e) {};
            //单搜列表
            let list = "";
            try {
                list = /fanqianxs/.test(MY_URL) ? JSON.parse(html).data.search : /midukanshu|17k|pigqq|leeyegy/.test(MY_URL) ? JSON.parse(html).data : pdfa(html, 列表);
            log(list)
            } catch (e) {
                list = "";
            };
            if (list == "") {
                d.push({
                    title: "‘‘’’<small>－－到\t底\t了－－</small>",
                    url: "hiker://empty",
                    col_type: "text_center_1"
                })
            } else
            if (/fanqianxs|api|app/.test(MY_URL)) {
                for (let j in list) {
                    if (/fanqianxs/.test(MY_URL)) {
                        名1 = list[j].book_name;
                        作1 = list[j].author;
                        更1 = list[j].latest_chapter_name;
                        类1 = list[j].cate_name;
                        img1 = MY_HOME + list[j].cover;
                        介1 = list[j].intro;
                        surl1 = MY_HOME + list[j].book_list_url;
                    } else
                    if (/pigqq|leeyegy/.test(MY_URL)) {
                        名1 = list[j].Name;
                        作1 = list[j].Author;
                        更1 = list[j].LastChapter;
                        类1 = list[j].CName;
                        img1 = list[j].Img;
                        介1 = list[j].Desc;
                        surl1 = (/leeyegy/.test(MY_URL) ? "https://scxs.pysmei.com" : "https://infosxs.pigqq.com") + "/BookFiles/Html/" + parseInt(list[j].Id / 1000 + 1) + "/" + list[j].Id + "/index.html";
                    } else
                    if (/17k/.test(MY_URL)) {
                        名1 = list[j].book_name;
                        作1 = list[j].author_name;
                        更1 = list[j].last_update_chapter_name;
                        类1 = list[j].category_name_2;
                        img1 = list[j].cover;
                        介1 = list[j].intro;
                        surl1 = "http://api.17k.com/v2/book/" + list[j].book_id + "/volumes?app_key=4037465544&price_extend=1&_versions=979&client_type=2&_filter_data=1&channel=2&merchant=17Khwyysd&_access_version=2&cps=0";
                    } else {
                        名1 = list[j].title;
                        作1 = list[j].author;
                        更1 = "更新至第" + list[j].chapterNum + "章";
                        类1 = list[j].category;
                        img1 = list[j].cover;
                        介1 = list[j].description;
                        surl1 = "https://book.midukanshu.com/book/chapter_list/100/" + list[j].book_id + ".txt";
                    }
                    let 名 = 名1
                    let 作 = "作者：" + 作1;
                    let 更 = "状态：" + 更1;
                    let 类 = "类型：" + 类1;
                    let img = img1;
                    let 介 = "简介：" + 介1;
                    let surl = surl1;
                    let url = $('#noLoading#').lazyRule((url) => {
                        putMyVar('bak', '1');
                        return $(url).rule(() => {
                            require(config.依赖);
                            erji()
                        })
                    }, "hiker://empty##" + surl + '#immersiveTheme##autoCache##noHistory' + game);
                    let extra = {
                        title: 名,
                        desc: 更,
                        img: img,
                        zuo: 作,
                        lei: 类,
                        jie: 介,
                        bookid: /17k/.test(MY_URL) ? list[j].book_id : /pigqq|leeyegy|jiaston/.test(MY_URL) ? list[j].Id : "",
                        bkid: /pigqq|leeyegy/.test(MY_URL) ? parseInt(list[j].Id / 1000 + 1) : "",
                        pageTitle: 名 + "（" + name + "）"
                    };
                    let title = slie ? "‘‘’’<b>" + 名 + '</b><br><small><font color="#FA7298">' + 作 + "</font></small>" : 名;
                    let desc = slie ? '‘‘’’<font color= "#274c5e">' + 类 + "<br>" + 更 + "<br>" + 介 + "</font>" : 更;
                    let jin = getSearchMode() == "1";

                    if (jin) {
                        if (searchContains(title, sokey, false)) {
                            if (名 == sokey) {
                                d.push({
                                    title: title,
                                    desc: desc,
                                    img: img,
                                    url: url,
                                    extra: extra,
                                    col_type: col
                                });
                            }
                        };
                    } else {
                        if (searchContains(title, sokey, false)) {
                            d.push({
                                title: title,
                                desc: desc,
                                img: img,
                                url: url,
                                extra: extra,
                                col_type: col
                            })
                        }
                    }
                }
            } else {
                for (let j in list) {
                    let picc = pdfh(list[j], 图片);
                    let 名 = pdfh(list[j], 片名);
                    let 作 = "作者：" + pdfh(list[j], 作者).replace(/\[|\]|作者|：|:/g, "");
                    let 类 = "类型：" + pdfh(list[j], 类型).replace(/类型|:|：/g, "");
                    let 介 = "简介：" + pdfh(list[j], 简介).replace(/简介|：|:/g, "");
                    let 更 = "状态：" + pdfh(list[j], 更新).replace(/状态|最新|章节|：|:/g, "");
                    let title = slie ? "‘‘’’<b>" + 名 + '</b><br><small><font color="#FA7298">' + 作 + "</font></small>" : 名;
                    let desc = slie ? '‘‘’’<font color= "#274c5e">' + 类 + "<br>" + 更 + "<br>" + 介 + "</font>" : 更; //log(desc)
                    let img = /.*url\=http/.test(picc) ? picc.replace(/.*url=/g, "") : (!/http|pic|jpg|png|jpeg/.test(picc)) ? http + 'tu/fy.png' : pd(list[j], 图片, ssurl) + "@Referer=";
                    let sur = pd(list[j], 链接, ssurl);
                    let surl = /ikanshuba/.test(MY_URL) ? sur + "dir.html" : /caimoge/.test(MY_URL) ? sur.replace(/\/txt\/(\d+)\.html/g, "/indexlist/$1/") : /xs7/.test(MY_URL) ? sur.replace(/info-/g, "shu/").replace(/.html/g, "/") : /75zhongwen/.test(MY_URL) ? sur.replace(/xiaoshuo/g, "").replace(/\.html/g, "/") : sur;
                    let url = $('#noLoading#').lazyRule((url) => {
                        putMyVar('bak', '1');
                        return $(url).rule(() => {
                            require(config.依赖);
                            erji()
                        })
                    }, "hiker://empty##" + surl + '#immersiveTheme##autoCache##noHistory' + game);
                    let extra = {
                        title: 名,
                        desc: 更,
                        img: img,
                        zuo: 作,
                        lei: 类,
                        jie: 介,
                        pageTitle: 名 + "（" + name + "）"

                    };

                    let jin = getSearchMode() == "1";

                    if (jin) {
                        if (searchContains(title, sokey, false)) {
                            if (名 == sokey) {
                                d.push({
                                    title: title,
                                    desc: desc,
                                    img: img,
                                    url: url,
                                    extra: extra,
                                    col_type: col
                                });
                            }
                        };
                    } else {
                        if (searchContains(title, sokey, false)) {
                            d.push({
                                title: title,
                                desc: desc,
                                img: img,
                                url: url,
                                extra: extra,
                                col_type: col
                            })
                        }
                    }
                }
            }
        } catch (e) {
            log(e.toString());
        }

    }

    try {
        if (!jusou) {
            search(selected, ssurl, d, MY_PAGE);
            setResult(d);
        } else {
            //聚搜
            let 线程数 = 8;
            let Data = [];
            for (let i = (MY_PAGE - 1) * 线程数; i < rn.length && i < MY_PAGE * 线程数; i++) {
                Data.push(rn[i]);
            }
            if (Data.length <= 0) {
                setResult(d);
            } else {
                let pageid = "__app0" + MY_PAGE;
                d.push({
                    title: "正在加载中第" + MY_PAGE + "页，进度：1/" + Data.length,
                    url: "hiker://empty",
                    col_type: "text_center_1",
                    desc: "",
                    pic_url: "",
                    extra: {
                        id: pageid
                    }
                });
                setResult(d);
                let tasks = [];
                for (let k in Data) {
                    let it = Data[k];
                    tasks.push({
                        func: function(param) {
                            let d = [];
                            search(param.n, param.u, d, 1);
                            return {
                                d: d,
                                n: param.n
                            };
                        },
                        param: {
                            u: rules[it].ssurl,
                            n: it
                        }
                    });
                };
                batchExecute(tasks, {
                    func: function(param, id, error, result) {
                        //log("listener: " + (result || []).length);
                        param.i = param.i + 1;
                        if (result) {
                            let d = [];
                            d.push({
                                title: result.n + " (" + (result.d.length <= 0 ? "无结果" : result.d.length) + ")",
                                url: "hiker://empty",
                                col_type: "text_1",
                                extra: {
                                    lineVisible: false
                                }
                            });
                            for (let it of result.d) {
                                param.j = param.j + 1;
                                d.push(it);
                            }
                            addItemBefore(pageid, d);
                        };
                        if (param.i >= param.all) {
                            deleteItem(pageid)
                        } else {
                            updateItem({
                                title: "正在加载第" + MY_PAGE + "页，进度：" + (param.i + 1) + "/" + param.all,
                                url: "hiker://empty",
                                col_type: "text_center_1",
                                desc: "",
                                pic_url: "",
                                extra: {
                                    id: pageid
                                }
                            })
                        }
                    },
                    param: {
                        all: Data.length,
                        i: 0,
                        j: -1
                    }
                });
            };
        };
    } catch (e) {
        log(e.toString());
        setResult(d);
    }
} else {
    d.push({
        title: '<span style="color:#ff6600"><b>\t热搜榜\t\t\t</b></span>',
        url: "hiker://empty",
        pic_url: http + "tu/reso.jpg",
        col_type: "avatar"
    });

    //let rso = "https://waptv.sogou.com/hotsugg";

    if (fileExist("hiker://files/cache/Yue/lt.js")) {
        eval("LT = " + readFile("hiker://files/cache/Yue/lt.js"), 0);
    };
    if (!fileExist("hiker://files/cache/Yue/reso.js") || CT > (LT + 2 * 24 * 60 * 60 * 1000)) {
        let rurl = "https://top.baidu.com/api/board?platform=wise&tab=novel&tag=%7B%7D";
        let rsurl = JSON.parse(request(rurl)).data.cards[0].content;
        saveFile("hiker://files/cache/Yue/reso.js", JSON.stringify(rsurl), 0);
        saveFile("hiker://files/cache/Yue/lt.js", JSON.stringify(CT), 0);
    };

    eval("listr = " + readFile("hiker://files/cache/Yue/reso.js"), 0); //log(listr)
    for (let r in listr) {
        let pic = listr[r].img;
        let 名 = listr[r].word;
        let 作 = listr[r].show[0];
        let 类 = listr[r].show[1];
        let 状1 = listr[r].show[2];
        let 指 = listr[r].hotScore;
        let 状 = /连载/.test(状1) ? '<font color="#00cc99">' + 状1 + '</font>' : /完结/.test(状1) ? '<font color="#1e90ff">' + 状1 + '</font>' : 状1;
        let 排 = (parseInt(r) + parseInt(1));
        let 数 = 排 == 1 ? '<span style="color:#ff1100">' + 排 + '</span>' : 排 == 2 ? '<span style="color:#ff8800">' + 排 + '</span>' : 排 == 3 ? '<span style="color:#FFA500">' + 排 + '</span>' : 排 == 4 ? '<span style="color:#ffcc66">' + 排 + '</span>' : 排 == 5 ? '<span style="color:#f0e68c">' + 排 + '</span>' : '<span style="color:#999999">' + 排 + '</span>';
        d.push({
            title: "‘‘’’<b>" + 数 + "\t" + 名 + "</b><br>" + '\t\t<small><font color="#FA7298">' + 作 + "</font></small>",
            desc: '‘‘’’\t\t' + 状 + '<br>\t\t<font color= "#274c5e">' + 类 + '<br>\t\t热搜指数：' + 指 + "</font>",
            img: pic,
            url: $(名 + '#noLoading#').lazyRule(() => {
                require(config.依赖);
                return sourl()
            }),
            col_type: "movie_1_vertical_pic"
        })
    }
    setResult(d);
}

//by随风
//搜索验证出自墙佬
//聚搜模板