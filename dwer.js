js:
let headers = {
    "User-Agent": MOBILE_UA,
    "Referer": MY_URL,
};
let ht = fetch(MY_URL, {
        timeout: 3000,
        headers: headers
    });
//log(ht)
let html = "";
if (ht.indexOf('检测中') != -1) {
    html = request(MY_HOME + "?btwaf" + ht.match(/btwaf(.*?)\"/)[1], {
        headers: headers
    });
} else
if (ht == "" || !/小说|连载|完结|作者|同人|玄幻|都市|言情|网游|科幻|第|章|1|2|3|一|二|三/.test(ht)) {
    toast("没有数据可用，请切换首页重试");
} else {
    html = ht;
};
//log(html)
//分页
let 分列 = "";
let 分链 = "";
if (/woo15/.test(MY_URL)) {
    分列 = '.pagelist&&li'
    分链 = 'a||span&&href||Text'
} else 
if (/listpage|pagenum/.test(html)) {
    分列 = '.listpage||.pagenum&&select&&option'
    分链 = 'option&&value'
} else
if (/auete/.test(MY_URL)) {
    分列 = 'body&&h2'
    分链 = 'body&&Text'
} 

//列表
var 列表 = "";
if (/woo15/.test(MY_URL)) {
    列表 = '.chapters,1&&li'
} else 
if (/chapterList|list_xm|list-group/.test(html)) {
    列表 = '#chapterList||.list_xm,1||.list-group&&li'
} else 
if (/chapter-list/.test(html)) {
    列表 = 'body&&.chapter-list'
} else
if (/chapter|read/.test(html)) {
    列表 = '.chapter,1||.chapter||.read&&li'
} else 
if (!/pigqq|leeyegy/.test(MY_URL)) {
    列表 = '.clear,5&&div:gt(1)'
}
