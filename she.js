js:
var d = [];
d.push({
    col_type: "line_blank"
})
let bturl = "https://lanmeiguojiang.com/tubiao/q/";
d.push({
    title: "<b><small>功能</small></b>",
    url: "hiker://empty",
    col_type: "avatar",
    img: bturl + "94.png",
});

let ur = "https://lanmeiguojiang.com/tubiao/messy/";
let coll = "";
if (MY_NAME == "海阔视界" && getAppVersion() >= "3475" || MY_NAME == "嗅觉浏览器" && getAppVersion() >= "859") {
    coll = "text_icon";
} else {
    coll = "avatar";
}
let 屏 = "二级页面顶部以全屏显示，右上角为菜单按钮，设置后，需返回下拉刷新生效。";

let 名称 = ["顶部全屏"];
let 储存 = ["全屏"];
let des = [屏];

for (let i in 储存) {
    d.push({
        title: "<b>" + 名称[i] + "</b>",
        url: $("#noLoading#").lazyRule((储存) => {
            if (getItem(储存, "off") == "on") {
                setItem(储存, "off");
            } else {
                setItem(储存, "on");
            };
            refreshPage(false);
            return "hiker://empty";
        }, 储存[i]),
        img: getItem(储存[i], 储存[i] == "首页" ? "on" : "off") == "on" ? ur + "55.svg" : ur + "63.svg",
        col_type: coll,
        extra: {
            longClick: [{
                title: des[i],
                js: $.toString((des) => {
                    return "toast://" + des;
                }, des[i])
            }]
        }
    });
};

d.push({
    col_type: "line_blank"
})
/*
d.push({
    title: "<b><small>阅读</small></b>",
    url: "hiker://empty",
    col_type: "avatar",
    img: bturl + "106.png",
});
let lmtu = "https://lanmeiguojiang.com/tubiao/q/";

let se = {
    "灰": "#777777",
    "浅": "#ADADAD",
    "淡": "#DCDFE1",
    "蓝": "#90BFF5",
    "绿": "#88C16F",
    "粉": "#F6AEAE",
};
let title = Object.keys(se);
let url = Object.values(se);
let xian = getItem("se", "灰");

let Color = [];
let bia = title.map((tit, index) => {
    if (tit == xian) {
        for (let i in url) {
            if (index == i) {
                Color.push(url[i]);
                setItem("Color", url[i]);
            };
        };
    };
    return tit == xian ? '‘‘’’<b><span style="color: ' + Color + '">' + tit + '色</span></b>' : tit + "色";
});

d.push({
    title: '<b><span style="color: ' + Color + '">字体换色（' + xian + '）</span></b>',
    url: $(bia, 3, "字体换色").select(() => {
        setItem("se", input.match(/[\u4e00-\u9fa5]/)[0]);
        refreshPage(false);
        return "hiker://empty"
    }),
    img: "https://lanmeiguojiang.com/tubiao/ke/147.png",
    col_type: coll
});

d.push({
    col_type: "line_blank"
}) */
d.push({
    title: "<b><small>更新</small></b>",
    url: "hiker://empty",
    col_type: "avatar",
    img: bturl + "97.png",
});
d.push({
    title: "<b>自动更新</b>",
    url: $("#noLoading#").lazyRule(() => {
        if (getItem("自动", "on") == "on") {
            setItem("自动", "off");
        } else {
            clearItem("自动");
        };
        refreshPage(false);
        return "hiker://empty";
    }),
    img: getItem("自动", "on") == "on" ? ur + "55.svg" : ur + "63.svg",
    col_type: coll
});

let 检 = "手动检测更新，无需频繁点击，频繁点击会禁用检测更新。";
let item = CT > (parseInt(getMyVar("时间", "0").replace("CT", "")) + 0.2 * 60 * 60 * 1000);
d.push({
    title: "<b>检测更新</b>",
    url: $("#noLoading#").lazyRule((CT, item) => {
        let toa = "";
        if (item) {
            putMyVar("时间", CT);
            putMyVar("手动", "1");
            require(config.依赖);
            gx();
            toa = "hiker://empty";
        } else {
            toa = "toast://检测更新过于频繁，已禁止检测";
        };
        refreshPage(false);
        return toa;
    }, CT + "CT", item),
    img: ur + "119.svg",
    col_type: coll,
    extra: {
        longClick: [{
            title: 检,
            js: $.toString((des) => {
                return "toast://" + des;
            }, 检)
        }]
    }
});

d.push({
    col_type: "line_blank"
})

d.push({
    title: "<b><small>其它</small></b>",
    img: bturl + "92.png",
    url: "hiker://empty",
    col_type: "avatar",
});

let tile = ["关于风阅", "清除数据"];
let 关于 = $("hiker://empty#noRecordHistory##noHistory##noRefresh#").rule((CT, http) => {
    let d = [];
    d.push({
        url: "hiker://empty",
        col_type: "card_pic_3",
        extra: {
            longClick: [{
                    title: getItem("隐藏", "off") == "on" ? "状态：显示" : "状态：隐藏",
                    js: $.toString(() => {
                        if (getItem("隐藏", "off") == "on") {
                            clearItem("隐藏");
                        } else {
                            setItem("隐藏", "on");
                        };
                        refreshPage(false);
                        return getItem("隐藏", "off") == "on" ? "toast://已显示" : "toast://已隐藏";
                    })
                }
                /*, {
                                title: "OFF",
                                js: $.toString(() => {
                                    setItem("隐藏", "off");
                                    return "toast://已关闭";
                                })
                            }*/
            ]
        }
    })
    d.push({
        title: "‘‘’’<b>\t\t版本号: " + getItem("当前", "1.0") + "</b>",
        url: "hiker://empty",
        img: base64Decode(http) + "tu/fy.png",
        col_type: "card_pic_3"
    })
    d.push({
        col_type: "line_blank"
    })

    let items = CT > (parseInt(getItem("强时", "0").replace("CT", "")) + 6 * 60 * 60 * 1000);
    d.push({
        title: "<b>强制更新</b>",
        url: $("#noLoading#").lazyRule((items, CT) => {
            let toa = "";
            if (items) {
                confirm({
                    title: "重要提示",
                    content: "强制更新专为小程序无法更新或更新异常时使用(更新完成需返回首页刷新页面)\n确认将会清除全部数据",
                    confirm: $.toString((CT) => {
                        setItem("强时", CT);
                        putMyVar("强制", "1");
                        require(config.依赖);
                        gx();
                        refreshPage(false);
                    }, CT),
                    cancel: $.toString(() => {
                        toast("强制更新取消");
                    })
                });
                toa = "hiker://empty";
            } else {
                toa = "toast://强制更新为备用，已禁用";
            };
            return toa;
        }, items, CT + "CT"),
        img: "https://lanmeiguojiang.com/tubiao/messy/156.svg",
        col_type: "text_icon"
    })
    d.push({
        col_type: "line_blank"
    })
    d.push({
        title: "‘‘’’<b>免责声明</b>",
        url: "hiker://empty",
        col_type: "text_1",
        extra: {
            lineVisible: false
        }
    })

    d.push({
        title: "1、小程序数据内容均来源于互联网，经软件对原网页源码重新排版后显示，此小程序与海阔不参与任何制作、上传、储存等内容，其显示的所有阅读内容，其版权均归原网站作者所有。<br>2、此小程序代码内容仅供爱好者学习与交流使用，禁止用于其他用途，请于导入后24小时内删除，请勿传播！<br>3、因使用此小程序产生的版权问题，软件开发者与此小程序作者概不负责。",
        col_type: "rich_text",
        extra: {
            textSize: "13"
        }
    });
    setResult(d);
}, CT, base64Encode(http));

let itemi = CT > (parseInt(getItem("时", "0").replace("CT", "")) + 24 * 60 * 60 * 1000);
let 清除 = $("#noLoading#").lazyRule((item, CT) => {
    let toa = "";
    if (item) {
        putMyVar("清除", "1");
        setItem("时", CT);
        toa = "hiker://empty";
        require(config.依赖);
        gx();
    } else {
        toa = "toast://清除完成";
    };
    refreshPage(false);
    return toa;
}, itemi, CT + "CT");
let urlg = [关于, 清除];
let png = ["tu/zhuan.png", "tu/qing.png"];
let 清 = "清除二级页面缓存数据(不要频繁清除，清除后进入二级会变慢)，每隔30天自动清除一次。";
let zd = "https://lanmeiguojiang.com/tubiao/system/10.png";
for (let i in tile) {
    let extra = i == 1 ? {
        longClick: [{
            title: 清,
            js: $.toString((des) => {
                return "toast://" + des;
            }, 清)
        }]
    } : {
        inheritTitle: false
    };
    d.push({
        title: "<b>" + tile[i] + "</b>",
        url: urlg[i],
        col_type: coll,
        img: i == 0 ? zd : http + "tu/qing.png",
        extra: extra
    });
};

d.push({
    col_type: "line_blank"
})
d.push({
    title: "<b>版本: " + getItem("当前", "1.0") + "</b>",
    img: http + "tu/fy.png",
    url: "hiker://empty",
    col_type: "avatar",
});
let 内容 = "";
try {
    eval("内容 = " + readFile("hiker://files/cache/Yue/nr.js"), 0);
} catch (e) {
    内容 = "1,风阅初始版本。"
};
d.push({
    title: "<b>更新日期：" + getItem("日期", "2023-03-27") + "<small>\t\t\tBy\t随风</small></b>" + 内容.replace("1,", "<br>1,").replace(/\n/g, "<br>"),
    col_type: "rich_text",
    extra: {
        textSize: "12"
    }
});

d.push({
    col_type: "line"
})
d.push({
    title: "<br>",
    col_type: "rich_text"
});
setResult(d);